"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

from PySide2.QtCore import QObject, Signal, Slot


class QPIDControlSignal(QObject):
    interfaced = Signal()

    def __init__(self):
        QObject.__init__(self)

    @Slot()
    def askToCreateGraph(self):
        self.interfaced.emit()
        print("[SIGNAL->EMITTED] => Call to draw graph")

    @Slot()
    def askToGenerateSamples(self):
        self.interfaced.emit()
        print("[SIGNAL->EMITTED] => Call to generate samples for sound play")