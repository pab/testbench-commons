"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

from math import log

from PySide2.QtGui import QDoubleValidator, Qt
from PySide2.QtWidgets import QGroupBox, QDial, QLineEdit, QVBoxLayout, QComboBox

from controls.QPIDControlSignal import QPIDControlSignal


class QPIDControl(QGroupBox):

    def __init__(self, controlName="Untitled Control", oscName="Untitled OSC",
                 spec=[-10, 4]):
        self.controlName = controlName.strip()
        self.oscName = oscName
        self.range = spec
        super(QPIDControl, self).__init__(title=self.controlName)

        self.controlValue = 0

        self.knob = QDial()
        self.knob.setMinimum(self.range[0])
        self.knob.setMaximum(self.range[1])
        self.knob.setValue(self.range[0])
        self.knob.setWrapping(False)

        self.display = QLineEdit()
        self.display.setValidator(QDoubleValidator())
        self.display.setPlaceholderText("Specify {} value".format(self.controlName))
        self.display.setAlignment(Qt.AlignCenter)

        self.drawGraphSignal = QPIDControlSignal()

        self.updateControlValueFromKnob()

        self.controlSource = QComboBox()
        self.controlSource.addItem("None")

        self.osc = QVBoxLayout()
        self.osc.addWidget(self.display, Qt.AlignCenter)
        self.osc.addWidget(self.knob, Qt.AlignCenter)
        self.osc.addWidget(self.controlSource, Qt.AlignCenter)

        self.setLayout(self.osc)

        self.knob.valueChanged.connect(lambda: self.updateControlValueFromKnob(graphDraw=True))
        self.display.textEdited.connect(lambda: self.updateControlValueFromDisplay(graphDraw=True))

    def fillInControlSources(self, controlSources):
        self.controlSource.addItems(controlSources)

    def setControlDisplay(self, text):
        self.display.blockSignals(True)
        self.display.setText(str(text))
        self.display.setAlignment(Qt.AlignCenter)
        self.display.blockSignals(False)

    def setControlKnob(self, value):
        self.knob.blockSignals(True)
        self.knob.setValue(value)
        self.knob.blockSignals(False)

    def updateControlValueFromKnob(self, graphDraw=False):
        self.controlValue = 10 ** (self.knob.value())
        self.setControlDisplay(self.controlValue)
        print("[{}->PARAMETER CHANGE] => {} set to {} from knob".format(self.oscName, self.controlName,
                                                                        self.controlValue))
        if graphDraw:
            self.drawGraphSignal.askToCreateGraph()

    def updateControlValueFromDisplay(self, graphDraw=False):
        self.controlValue = float(self.display.text())
        self.setControlKnob(log(self.controlValue + 0.000000001, 10))
        print("[{}->PARAMETER CHANGE] =>  {} set to {} from display".format(self.oscName, self.controlName,
                                                                            self.controlValue))
        if graphDraw:
            self.drawGraphSignal.askToCreateGraph()
