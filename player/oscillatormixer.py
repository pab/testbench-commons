"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

import numpy as np


def oscillatorMixer(oscillatorSamplesAndRatios):
    totalRatio = 0
    for oscillatorSampleAndRatio in oscillatorSamplesAndRatios:
        if len(oscillatorSampleAndRatio[0]) > 0:
            totalRatio += oscillatorSampleAndRatio[1]

    mixedOutput = np.asarray(oscillatorSamplesAndRatios[0][0]) * oscillatorSamplesAndRatios[0][1]
    for i in range(1, len(oscillatorSamplesAndRatios)):
        if len(oscillatorSamplesAndRatios[i][0]) > 0:
            mixedOutput += np.asarray(oscillatorSamplesAndRatios[i][0]) * oscillatorSamplesAndRatios[i][1]
    mixedOutput /= totalRatio

    return mixedOutput.tolist()
