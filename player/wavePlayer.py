"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

import scipy.io.wavfile

import pyaudio
import numpy as np
from matplotlib import pyplot as plt
from numpy.fft import fft, fftfreq


def playCEnvelopes(freq=440, gain=1, envelope_sampling_rate=1000, envelope_samples=None, name='demo'):
    p = pyaudio.PyAudio()

    samples = list()
    raw_samples = list()
    for i in range(int(len(envelope_samples) / envelope_sampling_rate * 44000)):
        raw_samples.append(0.8 * np.sin(np.pi / 22000 * freq * i))
        samples.append(envelope_samples[int(i * envelope_sampling_rate / 44000)] * raw_samples[i])

    samples = np.asarray(samples).astype(np.float32)
    raw_samples = np.asarray(raw_samples).astype(np.float32)

    stream = p.open(format=pyaudio.paFloat32,
                    channels=1,
                    rate=44000,
                    output=True)
    samples *= gain

    stream.write(samples)
    stream.stop_stream()
    stream.close()

    p.terminate()

    yf = fft(samples)
    xf = fftfreq(len(samples), 1 / 44000)

    # plt.plot(xf, np.abs(yf))
    plt.plot(samples)
    # plt.plot(raw_samples)
    # plt.show()

    scipy.io.wavfile.write("{}.wav".format(name), 44000, raw_samples)

    return samples


def playEnvelope(freq=440, gain=1, sampling_rate=44100, envelope_samples=None):
    p = pyaudio.PyAudio()

    samples = list()

    for i in range(len(envelope_samples)):
        samples.append(envelope_samples[i] * np.sin(2 * np.pi / sampling_rate * freq * i))

    samples = np.asarray(samples).astype(np.float32)

    stream = p.open(format=pyaudio.paFloat32,
                    channels=1,
                    rate=sampling_rate,
                    output=True)
    samples *= gain

    stream.write(samples)
    stream.stop_stream()
    stream.close()

    p.terminate()
    return samples


def playOscillator(oscillator_samples, freq=440, gain=1, sampling_rate=44100):
    p = pyaudio.PyAudio()

    samples = np.asarray(oscillator_samples).astype(np.float32) * gain

    # yf = fft(samples)
    # xf = fftfreq(len(samples), 1 / 44000)
    # plt.plot(xf, np.abs(yf))
    # plt.show()

    stream = p.open(format=pyaudio.paFloat32,
                    channels=1,
                    rate=sampling_rate,
                    output=True)
    stream.write(samples)
    stream.stop_stream()
    stream.close()
    p.terminate()

    # scipy.io.wavfile.write("AudioSamples/osc/1.wav", sampling_rate, samples)

    return samples


def play(play_data, sampling_rate=44100):
    p = pyaudio.PyAudio()
    samples = np.asarray(play_data).astype(np.float32)

    stream = p.open(format=pyaudio.paFloat32,
                    channels=1,
                    rate=sampling_rate,
                    output=True)
    stream.write(samples)
    stream.stop_stream()
    stream.close()
    p.terminate()
    return samples


if __name__ == '__main__':
    import pandas as pd

    name = 'Sound_Demo'
    data = pd.read_csv('/home/waohwoah/MEGA/MEGAsync/PIDE/PIDEG_Core_CSVs/{}.csv'.format(name))
    envelope_samples = data['A'].tolist()
    playCEnvelopes(freq=440, envelope_samples=envelope_samples, name='OriginalSine')
