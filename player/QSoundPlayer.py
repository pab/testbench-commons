"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

from PySide2.QtGui import QDoubleValidator, Qt, QIntValidator
from PySide2.QtWidgets import QGroupBox, QLineEdit, QHBoxLayout, QPushButton
from player import wavePlayer
from player.oscillatormixer import oscillatorMixer


class QSoundPlayer(QGroupBox):
    def __init__(self, soundDrivers):
        super(QSoundPlayer, self).__init__("Untitled Wave Player")

        self.soundDrivers = soundDrivers

        self.freqControl = QLineEdit()
        self.freqControl.setValidator(QIntValidator())
        self.freqControl.setPlaceholderText("Specify Frequency(in Hz)")
        self.freqControl.setAlignment(Qt.AlignCenter)

        self.durnControl = QLineEdit()
        self.durnControl.setValidator(QDoubleValidator())
        self.durnControl.setPlaceholderText("Specify Duration(in s)")
        self.durnControl.setAlignment(Qt.AlignCenter)

        self.srateControl = QLineEdit()
        self.srateControl.setValidator(QIntValidator())
        self.srateControl.setPlaceholderText("Specify SRate(in Hz)")
        self.srateControl.setAlignment(Qt.AlignCenter)

        self.srateDownScalingFactor = 10
        self.totalSimulationSamples = 0

        self.gainControl = QLineEdit()
        self.gainControl.setValidator(QDoubleValidator())
        self.gainControl.setPlaceholderText("Specify Gain(0-1)")
        self.gainControl.setAlignment(Qt.AlignCenter)

        self.playButton = QPushButton()

        self.layout = QHBoxLayout()

        self.layout.addWidget(self.freqControl)
        self.layout.addWidget(self.durnControl)
        self.layout.addWidget(self.srateControl)
        self.layout.addWidget(self.gainControl)
        self.layout.addWidget(self.playButton)
        self.setLayout(self.layout)

        self.freqControl.textChanged.connect(self.updateOscFreq)
        self.durnControl.textChanged.connect(self.updateSimulationSampleCount)
        self.srateControl.textChanged.connect(self.updateSimulationSampleCount)

        self.freqControl.setText("440")
        self.durnControl.setText("12")
        self.srateControl.setText("44100")
        self.gainControl.setText("1")
        self.playButton.setText("Play")

        self.playButton.clicked.connect(self.playSound)

    def playSound(self):
        if self.isEnvPlayer():
            upScaledEnvelope = self.magnifyEnvelopeToLength(envelope=self.soundDrivers.envOutputSamples,
                                                            final_length=self.totalSimulationSamples)
            playedSamples = wavePlayer.playEnvelope(freq=int(self.freqControl.text()),
                                                    sampling_rate=int(self.srateControl.text()),
                                                    gain=int(self.gainControl.text()),
                                                    envelope_samples=upScaledEnvelope)
        else:
            oscillatorSamplesAndRatios = list()
            for driver in self.soundDrivers:
                oscillatorSamplesAndRatios.append([driver.givePlayerSamples(frequency=int(self.freqControl.text()),
                                                                            sampling_rate=int(self.srateControl.text()),
                                                                            duration_s=int(self.durnControl.text())),
                                                   driver.controls['Ratio'].controlValue])
            mixedSamples = oscillatorMixer(oscillatorSamplesAndRatios=oscillatorSamplesAndRatios)
            wavePlayer.playOscillator(freq=int(self.freqControl.text()),
                                      sampling_rate=int(self.srateControl.text()),
                                      gain=int(self.gainControl.text()),
                                      oscillator_samples=mixedSamples)

    def updateSimulationSampleCount(self):
        try:
            self.totalSimulationSamples = int(self.durnControl.text()) * int(self.srateControl.text())
        except:
            print("[PASS->PLAYER] => Total Simulation Sample Count not set")
            return
        print("[PLAYER] => Total Simulation Sample Count set to {}".format(self.totalSimulationSamples))

    def magnifyEnvelopeToLength(self, envelope, final_length):
        intersample = list()
        og_length = len(envelope)
        for i in range(final_length):
            intersample.append(envelope[int(i / (final_length / og_length))])
        for i in range(final_length - len(intersample)):
            intersample.append(0.0)
        return intersample

    def isEnvPlayer(self):
        if type(self.soundDrivers).__name__ == 'QPIDEnvelope':
            return True
        return False

    def updateOscFreq(self):
        if not self.isEnvPlayer():
            for driver in self.soundDrivers:
                driver.setCurrentFrequency(frequency=int(self.freqControl.text()))
                driver.updateOscGraphing()
            print("[PLAYER] => Updated all oscillator plotting frequency to {}Hz".format(self.freqControl.text()))
