# Testbench Common Dependencies

Collection of python packages required by testbench GUIs for PIDEG and PIDS. 

Contains modules for:

- PID and oversampling algorithms
- Control GUI definitions and their interactions
- Plot GUI definitions
- Sound player and its GUI definitions
- Dark GUI Theme definitions

This repo is added as submodules to [PIDEG Testbench](https://gitlab.com/pid-envelope/pideg-testbench) and [PIDS Testbench](https://gitlab.com/pidosc/pids-testbench) projects.

## Contributing

1. [Report Bugs or Request Features](https://gitlab.com/pab/testbench-commons/)
2. Fork it (<https://github.com/yourname/yourproject/fork>)
3. Create your feature branch (`git checkout -b feature/fooBar`)
4. Commit your changes (`git commit -am 'Add some fooBar'`)
5. Push to the branch (`git push origin feature/fooBar`)
6. Create a new Merge/Pull Request

## License

Distributed under the GNU General Public License v3.0 License. See `LICENSE` for more information.
