"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

import numpy as np
from PySide2.QtWidgets import QVBoxLayout, QWidget
import pyqtgraph as pg
import random

from scipy.fft import fft, fftfreq
from scipy.signal.windows import blackman


class QGrapher(QWidget):

    def __init__(self, samplingRate=44100):
        super(QGrapher, self).__init__()

        self.graphWidget = pg.PlotWidget()
        graph_layout = QVBoxLayout()
        graph_layout.addWidget(self.graphWidget)
        self.setLayout(graph_layout)

        self.MAX_BUFFER_SIZE = 2000

        self.curves = dict()
        self.samplingRate = samplingRate

    def set_graph(self, src, y, isLog=False):
        if src not in self.curves.keys():
            print("[GRAPHING->TIME] => Previously unassigned curve name = {}: Generating new curve...".format(src))
            self.set_plot(src.strip(), isLog=isLog)

        self.curves[src][1] = y
        self.curves[src][0].setData(self.curves[src][1])
        pg.QtGui.QApplication.processEvents()

    def set_freq_graph(self, src, y, isLog=True):
        if src not in self.curves.keys():
            print("[GRAPHING->TIME] => Previously unassigned curve name = {}: Generating new curve...".format(src))
            self.set_plot(src.strip(), isLog=isLog)

        N = len(y)
        w = blackman(N)

        ywf = fft(y * w)
        xf = fftfreq(N, 1.0 / self.samplingRate)[:N // 2]

        self.curves[src][1] = y
        self.curves[src][0].setData(xf[1:N // 2], 2.0 / N * np.abs(ywf[1:N // 2]))
        pg.QtGui.QApplication.processEvents()

    def append_graph(self, src, element_y, isLog=False):
        if src not in self.curves.keys():
            print("[GRAPHING->TIME] => Previously unassigned curve name = {}: Generating new curve...".format(src))
            self.set_plot(src.strip(), isLog=isLog)

        self.curves[src][1].append(element_y)
        if len(self.curves[src][1]) > self.MAX_BUFFER_SIZE:
            self.curves[src][1] = self.curves[src][1][1:]
        self.curves[src][0].setData(self.curves[src][1])
        pg.QtGui.QApplication.processEvents()

    def set_plot(self, src, isLog=False):
        if src in self.curves.keys():
            print("[GRAPHING->TIME] => Resetting existing curve name = {}".format(src))
            del self.curves[src]

        pen = pg.mkPen(color=(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))
        self.curves[src] = [self.graphWidget.plot(pen=pen), list()]
        if isLog:
            self.curves[src][0].setLogMode(True, False)

    def remove_plot(self, src):
        if src not in self.curves.keys():
            print("[ERROR->GRAPHING->TIME] => Cannot delete unavailable curve = {}".format(src))
            return
        del self.curves[src]
