"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

class UpDownSampler:
    def __init__(self):
        self.x0 = 0
        self.x1 = 0
        self.x2 = 0
        self.x3 = 0
        self.x4 = 0
        self.x5 = 0
        self.x6 = 0
        self.x7 = 0
        self.x8 = 0
        self.x9 = 0

        self.y0 = 0
        self.y1 = 0
        self.y2 = 0
        self.y3 = 0
        self.y4 = 0
        self.y5 = 0
        self.y6 = 0
        self.y7 = 0
        self.y8 = 0
        self.y9 = 0
        self.y10 = 0
        self.y11 = 0
        self.y12 = 0
        self.y13 = 0
        self.y14 = 0
        self.y15 = 0
        self.y16 = 0
        self.y17 = 0
        self.y18 = 0

    def os_os2(self):
        self.y18 = self.y16
        self.y17 = self.y15
        self.y16 = self.y14
        self.y15 = self.y13
        self.y14 = self.y12
        self.y13 = self.y11
        self.y12 = self.y10
        self.y11 = self.y9
        self.y10 = self.y8
        self.y9 = self.y7
        self.y8 = self.y6
        self.y7 = self.y5
        self.y6 = self.y4
        self.y5 = self.y3
        self.y4 = self.y2
        self.y3 = self.y1
        self.y2 = self.y0

    def os_down2(self):
        return 0.0028 * (self.y0 + self.y18) - 0.0118 * (self.y2 + self.y16) \
               + 0.0334 * (self.y4 + self.y14) - 0.0849 * (self.y6 + self.y12) \
               + 0.3106 * (self.y8 + self.y10) + 0.5 * self.y9


class Antialiaser:
    def __init__(self, synth, oversample_by=4):
        self.synth = synth
        self.oversample = oversample_by

        self.x2sampler = UpDownSampler()
        self.x4sampler = UpDownSampler()
        self.x8sampler = UpDownSampler()

    def runx1(self):
        return self.synth.get_next_sample()

    def runx2(self):
        self.x2sampler.os_os2()
        setpoint, self.x2sampler.y1 = self.runx1()
        setpoint, self.x2sampler.y0 = self.runx1()
        return setpoint, self.x2sampler.os_down2()

    def runx4(self):
        self.x4sampler.os_os2()
        setpoint, self.x4sampler.y1 = self.runx2()
        setpoint, self.x4sampler.y0 = self.runx2()
        return setpoint, self.x4sampler.os_down2()

    def runx8(self):
        self.x8sampler.os_os2()
        setpoint, self.x8sampler.y1 = self.runx4()
        setpoint, self.x8sampler.y0 = self.runx4()
        return setpoint, self.x8sampler.os_down2()

    def pidSynthAntialiase(self):
        if self.oversample == 8:
            return self.runx8()
        elif self.oversample == 4:
            return self.runx4()
        elif self.oversample == 2:
            return self.runx2()
        else:
            return self.runx1()
