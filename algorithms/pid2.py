"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

from collections import deque

measured_value = 0
setpoint = 1.5
errors = deque(maxlen=3)
errors.append(0)
errors.append(0)
errors.append(0)
output = 0

Kp = 1.5
Ki = 12
Kd = 0

T = 1 / 44100

K1 = Kp + T * Ki / 2 + Kd / T
K2 = -Kp - 2 * Kd / T + T * Ki / 2
K3 = Kd / T

measured_values = list()
outputs = list()
for i in range(100):
    errors.append(setpoint - measured_value)
    output = output + K1 * errors[2] + K2 * errors[1] + K3 * errors[0]
    measured_value += output
    measured_values.append(measured_value)
    outputs.append(output)
    print("{}\t{}\t{}".format(setpoint, measured_value, output))

from matplotlib import pyplot as plt

plt.plot(measured_values, label="Measure Value")
plt.plot(outputs, label="PID Output")
plt.legend()
plt.show()
